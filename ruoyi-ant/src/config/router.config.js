// eslint-disable-next-line
import { UserLayout, BlankLayout } from '@/layouts'
/**
 * 这里不再保留原来默认的菜单asyncRouterMap
 * https://github.com/sendya/ant-design-pro-vue/blob/master/src/router/generator-routers.js
 */
export const defaultRouterMap = [

  {
    'title': 'menu.exception',
    'key': 'exception',
    'component': 'RouteView',
    'icon': 'warning',
    'children': [
      {
        'hiddenHeaderContent': true,
        'title': 'menu.exception.403',
        'key': 'error403'
      },
      {
        'hiddenHeaderContent': true,
        'title': 'menu.exception.404',
        'key': 'error404'
      },
      {
        'hiddenHeaderContent': true,
        'title': 'menu.exception.500',
        'key': 'error500'
      }
    ]
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      }
    ]
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }

]
